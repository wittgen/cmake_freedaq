if( "${CMAKE_VERSION}" VERSION_GREATER_EQUAL "3.12")
  cmake_policy(SET CMP0074 NEW)
  cmake_policy(SET CMP0094 NEW)
endif()

if( "${CMAKE_VERSION}" VERSION_GREATER_EQUAL "3.27")
  cmake_policy(SET CMP0144 OLD)
  cmake_policy(SET CMP0148 OLD)
endif()

include(CMakeParseArguments)
include(CMakePackageConfigHelpers)


#######################################################################
#
# tdaq_package([NO_HEADERS] [NO_DOCS])
#
# Every package should start with this at the top, e.g.:
#
#   tdaq_package()
#
# If a directory with the same name as the package name exists
# all header files from that directory are automatically installed.
#
# The variable TDAQ_PACKAGE_NAME is set to the name of the "current"
# package. The variables <package>_SOURCE_DIR and <package>_BINARY_DIR
# are injected into the parent scope pointing to this package's
# source and build directory rsp.
#
# The NO_HEADERS option means that no header files are automatically
# installed. Use tdaq_add_header_directory() to install them from
# a non-standard place.
#######################################################################

function(tdaq_package)
    cmake_parse_arguments(ARG "NO_HEADERS;NO_DOCS" "" "" ${ARGN})

    get_filename_component(TDAQ_PACKAGE_NAME ${CMAKE_CURRENT_SOURCE_DIR} NAME)

    set(TDAQ_PACKAGE_VERSION"UNKNOWN")
    if (EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/.git)
        execute_process(COMMAND git describe --always --tags
                WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
                OUTPUT_VARIABLE TDAQ_PACKAGE_VERSION
                OUTPUT_STRIP_TRAILING_WHITESPACE)
    elseif (EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/cmt/version.cmt)
        execute_process(COMMAND cat cmt/version.cmt
                WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
                OUTPUT_VARIABLE TDAQ_PACKAGE_VERSION
                OUTPUT_STRIP_TRAILING_WHITESPACE)
    endif ()

    message(STATUS "  ${TDAQ_PACKAGE_NAME} : ${TDAQ_PACKAGE_VERSION}")
    file(APPEND ${CMAKE_BINARY_DIR}/packages.txt "${TDAQ_PACKAGE_NAME} ${TDAQ_PACKAGE_VERSION}\n")

    set(CMAKE_DIRECTORY_LABELS ${TDAQ_PACKAGE_NAME} PARENT_SCOPE)

    # weird, just setting it in the parent scope is not eough to use it later on
    set(TDAQ_COMPONENT_NOARCH ${TDAQ_PACKAGE_NAME}_noarch-${CMAKE_PROJECT_VERSION}_${TDAQ_REVISION})
    set(TDAQ_COMPONENT_NOARCH ${TDAQ_PACKAGE_NAME}_noarch-${CMAKE_PROJECT_VERSION}_${TDAQ_REVISION} PARENT_SCOPE)
    set(TDAQ_COMPONENT_BINARY ${TDAQ_PACKAGE_NAME}_${BINARY_TAG}-${CMAKE_PROJECT_VERSION}_${TDAQ_REVISION})
    set(TDAQ_COMPONENT_BINARY ${TDAQ_PACKAGE_NAME}_${BINARY_TAG}-${CMAKE_PROJECT_VERSION}_${TDAQ_REVISION} PARENT_SCOPE)

    file(WRITE ${CMAKE_CURRENT_BINARY_DIR}/${TDAQ_PACKAGE_NAME}.version "${TDAQ_PACKAGE_VERSION}")
    install(FILES ${CMAKE_CURRENT_BINARY_DIR}/${TDAQ_PACKAGE_NAME}.version
            COMPONENT ${TDAQ_COMPONENT_NOARCH}
            DESTINATION share/cmake/pkg-versions
            OPTIONAL
    )

    set(TDAQ_PACKAGE_NAME ${TDAQ_PACKAGE_NAME} PARENT_SCOPE)
    set(TDAQ_PACKAGE_VERSION ${TDAQ_PACKAGE_VERSION} PARENT_SCOPE)
    set(TDAQ_PACKAGE_LABELS ${TDAQ_PACKAGE_NAME} PARENT_SCOPE)
    set(TDAQ_GENCONFIG_INCLUDES PARENT_SCOPE)
    add_definitions(-DTDAQ_PACKAGE_NAME=\"${TDAQ_PACKAGE_NAME}\")

    set(${TDAQ_PACKAGE_NAME}_SOURCE_DIR ${CMAKE_CURRENT_SOURCE_DIR} PARENT_SCOPE)
    set(${TDAQ_PACKAGE_NAME}_BINARY_DIR ${CMAKE_CURRENT_BINARY_DIR} PARENT_SCOPE)

    # do a dummy 'install' to trigger later creation of both binary and noarch RPMs:
    install(CODE "" COMPONENT ${TDAQ_COMPONENT_NOARCH})
    install(CODE "" COMPONENT ${TDAQ_COMPONENT_BINARY})

    if (NOT ARG_NO_HEADERS)
        if (IS_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}/${TDAQ_PACKAGE_NAME})
            install(DIRECTORY ${TDAQ_PACKAGE_NAME} COMPONENT ${TDAQ_COMPONENT_NOARCH} DESTINATION include FILES_MATCHING PATTERN "*.h" PATTERN "*.hpp" PATTERN ".git" EXCLUDE PATTERN ".svn" EXCLUDE)
        endif ()
    endif ()

    if (NOT ARG_NO_DOCS)
        if (EXISTS ${CMAKE_CURRENT_SOURCE_DIR}/README.md)
            install(FILES README.md COMPONENT ${TDAQ_COMPONENT_NOARCH} DESTINATION share/doc/${TDAQ_PACKAGE_NAME})
        endif ()
    endif ()

endfunction(tdaq_package)


#######################################################################
#
# tdaq_add_executable(exename src1 src2..
#                     [NOINSTALL]
#                     [INCLUDE_DIRECTORIES dir1 dir2..]
#                     [LINK_LIBRARIES lib1 lib2...]
#                     [DEFINITIONS A B=C ...]
#                     [OPTIONS -pthread ...]
#                     [OUTPUT_NAME new_name]
#
# Add an executable to the build and install it according in the TDAQ
# standard locations.
#
# The following additional options are available compared to the standard
# add_executable():
#
#    NOINSTALL           - option, if given the library will be build but not installed.
#    LINK_LIBRARIES      - a list of libraries to link against.
#    LINK_OPTIONS        - a list options to pass to the linker
#    INCLUDE_DIRECTORIES - a list of include directories to add
#    DEFINITIONS         - a list of preprocessor definitions to add
#    OPTIONS             - a list of compiler options to add
#    DEPENDS             - a list of additional dependencies
#    OUTPUT_NAME         - rename the executable when install it
#    DESCRIPTION         - add description to binary in OKS software repository
#
# The following options are not available compared to the TDAQ version
# taq_add_executable():
#
#    DAL                 - this is a DAL executable
#    ADD_TO_OKS          - add binary to OKS software repository
#
# You can specify PUBLIC, PRIVATE or INTERFACE, default is PRIVATE for these options.
#
#######################################################################
function(tdaq_add_executable name)

    set(BINOPTS_LINK_LIBRARIES)
    cmake_parse_arguments(BINOPTS "NOINSTALL" "OUTPUT_NAME;DESTINATION;DESCRIPTION" "LINK_LIBRARIES;INCLUDE_DIRECTORIES;DEFINITIONS;OPTIONS;DEPENDS;LINK_OPTIONS" ${ARGN})

    if (BINOPTS_DESTINATION)
        set(bin_dest ${BINOPTS_DESTINATION})
    elseif (TDAQ_BIN_DESTINATION)
        set(bin_dest ${TDAQ_BIN_DESTINATION})
    else ()
        set(bin_dest ${BINARY_TAG}/bin)
    endif ()

    set(srcs)
    foreach (f ${BINOPTS_UNPARSED_ARGUMENTS})
        set(out)
        file(GLOB out ${f})
        if (out)
            set(srcs ${srcs} ${out})
        else ()
            set(srcs ${srcs} ${f})
        endif ()
    endforeach ()

    add_executable(${name} ${srcs})
    if (BINOPTS_LINK_OPTIONS)
        target_link_options(${name} ${BINOPTS_LINK_OPTIONS})
    endif ()

    set_target_properties(${name} PROPERTIES LABELS "${TDAQ_PACKAGE_LABELS}")

    set(debug_name ${name}.debug)
    if (BINOPTS_OUTPUT_NAME)
        set(debug_name ${BINOPTS_OUTPUT_NAME}.debug)
        set_target_properties(${name} PROPERTIES RUNTIME_OUTPUT_NAME ${BINOPTS_OUTPUT_NAME})
    endif ()

    if (TDAQ_NO_DEBUG_INSTALL)
        add_custom_command(TARGET ${name} POST_BUILD
                COMMAND ${CMAKE_OBJCOPY} --strip-debug $<TARGET_FILE:${name}>)
    else ()
        add_custom_command(TARGET ${name} POST_BUILD
               COMMAND ${CMAKE_OBJCOPY} --only-keep-debug $<TARGET_FILE:${name}> ${CMAKE_CURRENT_BINARY_DIR}/${debug_name}
                COMMAND ${CMAKE_OBJCOPY} --strip-debug $<TARGET_FILE:${name}>
                COMMAND ${CMAKE_OBJCOPY} --add-gnu-debuglink=${debug_name} $<TARGET_FILE:${name}>)
    endif ()

    get_directory_property(clean_files ADDITIONAL_MAKE_CLEAN_FILES)
    list(APPEND clean_files ${CMAKE_CURRENT_BINARY_DIR}/${debug_name})
    set_directory_properties(PROPERTIES ADDITIONAL_MAKE_CLEAN_FILES "${clean_files}")


    if (NOT BINOPTS_NOINSTALL)
        set(TDAQ_TARGETS ${TDAQ_TARGETS} ${name} PARENT_SCOPE)
        install(TARGETS ${name} OPTIONAL COMPONENT ${TDAQ_COMPONENT_BINARY} EXPORT ${PROJECT_NAME} RUNTIME DESTINATION ${bin_dest})
        if (NOT TDAQ_NO_DEBUG_INSTALL)
            install(FILES ${CMAKE_CURRENT_BINARY_DIR}/${debug_name} OPTIONAL COMPONENT ${TDAQ_COMPONENT_BINARY} DESTINATION ${bin_dest}/.debug)
        endif ()
    endif ()

    if (BINOPTS_LINK_LIBRARIES)
        set(link_libraries ${BINOPTS_LINK_LIBRARIES})
        target_link_libraries(${name} PRIVATE ${link_libraries})
        target_include_directories(${name} ${TDAQ_EXT_INCLUDE} PRIVATE ${link_headers})
    endif ()

    if (BINOPTS_INCLUDE_DIRECTORIES)
        target_include_directories(${name} ${TDAQ_EXT_INCLUDE} PRIVATE ${link_headers})
    endif ()

    if (BINOPTS_DEFINITIONS)
        target_compile_definitions(${name} PRIVATE ${BINOPTS_DEFINITIONS})
    endif ()

    if (BINOPTS_OPTIONS)
        target_compile_options(${name} PRIVATE ${BINOPTS_OPTIONS})
    endif ()

    if (BINOPTS_DEPENDS)
        add_dependencies(${name} ${BINOPTS_DEPENDS})
    endif ()

endfunction(tdaq_add_executable)

#######################################################################
#
# tdaq_add_library(libname src1 src2...
#                  [NOINSTALL][DAL]
#                  [LINK_LIBRARIES lib1 lib2...]
#                  [INCLUDE_DIRECTORIES dir1 dir2...]
#                  [DEFINITIONS A B=C...]
#                  [OPTIONS -O3 ...])
#
# Use this function to add a library to the build. It will be automatically
# installed according to the standard TDAQ rules.
#
# Each <srcN> argument will be glob'ed before being added to the target source list.
# So you can use wild-card expression.
#
# The following additional options are available compared to the standard
# add_library():
#
#    NOINSTALL           - option, if given the library will be build but not installed
#    LINK_LIBRARIES      - a list of libraries to link against.
#    INCLUDE_DIRECTORIES - a list of include directories to add
#    DEFINITIONS         - a list of preprocessor definitions to add
#    OPTIONS             - a list of compiler options to add
#    DEPENDS             - a list of additional dependencies
#
# The last four options are also available by using the usual CMake functions:
#
# target_include_directories(), target_link_libraries(), target_compiler_definitions()
# and target_compiler_options()
#
# You can specify PUBLIC, PRIVATE or INTERFACE, default is PUBLIC for these options
# just as in standard CMake.
#
#######################################################################

function(tdaq_add_library name)
    set(LIBOPTS_LINK_LIBRARIES)
    cmake_parse_arguments(LIBOPTS "NOINSTALL;SHARED;STATIC;MODULE;OBJECTS;INTERFACE" "DESTINATION" "LINK_LIBRARIES;INCLUDE_DIRECTORIES;DEFINITIONS;OPTIONS;DEPENDS;CXX_MODULES;LINK_OPTIONS" ${ARGN})

    if (LIBOPTS_DESTINATION)
        set(lib_dest ${LIBOPTS_DESTINATION})
    elseif (TDAQ_LIB_DESTINATION)
        set(lib_dest ${TDAQ_LIB_DESTINATION})
    else ()
        set(lib_dest ${BINARY_TAG}/lib)
    endif ()

    set(libtype SHARED)
    if (LIBOPTS_STATIC)
        set(libtype STATIC)
    elseif (LIBOPTS_MODULE)
        set(libtype MODULE)
    elseif (LIBOPTS_OBJECTS)
        set(libtype OBJECTS)
    elseif (LIBOPTS_INTERFACE)
        set(libtype INTERFACE)
    endif ()

    set(srcs)
    set(includes)
    foreach (f ${LIBOPTS_UNPARSED_ARGUMENTS})
        if (${f} MATCHES ".*\\*.*")
            set(out)
            file(GLOB out ${f})
            set(srcs ${srcs} ${out})
        else ()
            # may be generated file, so just add
            set(srcs ${srcs} ${f})
        endif ()
    endforeach ()

    add_library(${name} ${libtype} ${srcs})
    if (LIBOPTS_LINK_OPTIONS)
        target_link_options(${name} ${LIBOPTS_LINK_OPTIONS})
    endif ()

    if (LIBOPTS_CXX_MODULES)
        target_sources(${name} PUBLIC FILE_SET CXX_MODULES FILES ${LIBOPTS_CXX_MODULES})
    endif ()

    if (NOT "${libtype}" STREQUAL "INTERFACE")
        set_target_properties(${name} PROPERTIES LABELS "${TDAQ_PACKAGE_LABELS}")
    endif ()

    if (NOT (${libtype} STREQUAL "INTERFACE" OR ${libtype} STREQUAL "STATIC"))
        if (TDAQ_NO_DEBUG_INSTALL)
            add_custom_command(TARGET ${name} POST_BUILD
                    COMMAND ${CMAKE_OBJCOPY} --strip-debug $<TARGET_FILE:${name}>)
        else ()
            add_custom_command(TARGET ${name} POST_BUILD
		    COMMAND ${CMAKE_OBJCOPY} --only-keep-debug $<TARGET_FILE:${name}> ${CMAKE_CURRENT_BINARY_DIR}/lib${name}.so.debug
                    COMMAND ${CMAKE_OBJCOPY} --strip-debug $<TARGET_FILE:${name}>
                    COMMAND ${CMAKE_OBJCOPY} --add-gnu-debuglink=lib${name}.so.debug $<TARGET_FILE:${name}>
                    WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR})
            get_directory_property(clean_files ADDITIONAL_MAKE_CLEAN_FILES)
            list(APPEND clean_files ${CMAKE_CURRENT_BINARY_DIR}/lib${name}.so.debug)
            set_directory_properties(PROPERTIES ADDITIONAL_MAKE_CLEAN_FILES "${clean_files}")
        endif ()
    endif ()

    if (NOT LIBOPTS_NOINSTALL)
        set(TDAQ_TARGETS ${TDAQ_TARGETS} ${name} PARENT_SCOPE)
        install(TARGETS ${name} OPTIONAL COMPONENT ${TDAQ_COMPONENT_BINARY} EXPORT ${PROJECT_NAME} LIBRARY DESTINATION ${lib_dest} ARCHIVE DESTINATION ${lib_dest})
        if (NOT (${libtype} STREQUAL "INTERFACE" OR ${libtype} STREQUAL "STATIC"))
            if (NOT TDAQ_NO_DEBUG_INSTALL)
                install(FILES ${CMAKE_CURRENT_BINARY_DIR}/lib${name}.so.debug OPTIONAL COMPONENT ${TDAQ_COMPONENT_BINARY} DESTINATION ${lib_dest}/.debug)
            endif ()
        endif ()
    endif ()
    if (LIBOPTS_INTERFACE)
        target_include_directories(${name} ${TDAQ_EXT_INCLUDE} INTERFACE $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}> ${includes})
    else ()
        target_include_directories(${name} ${TDAQ_EXT_INCLUDE} PUBLIC $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}> ${includes})
    endif ()


    if (LIBOPTS_INCLUDE_DIRECTORIES)
        if (${libtype} STREQUAL "INTERFACE")
            target_include_directories(${name} ${TDAQ_EXT_INCLUDE} INTERFACE ${include_headers})
        else ()
            target_include_directories(${name} ${TDAQ_EXT_INCLUDE} PUBLIC ${include_headers})
        endif ()
    endif ()
    if (LIBOPTS_LINK_LIBRARIES)
        set(link_libraries ${BINOPTS_LINK_LIBRARIES})
        if (${libtype} STREQUAL "INTERFACE")
            target_link_libraries(${name} INTERFACE ${link_libraries})
        else ()
            target_link_libraries(${name} PUBLIC ${link_libraries})
        endif ()
        if (NOT "${link_headers}" STREQUAL "")
            if (${libtype} STREQUAL "INTERFACE")
                target_include_directories(${name} ${TDAQ_EXT_INCLUDE} INTERFACE ${link_headers})
            else ()
                target_include_directories(${name} ${TDAQ_EXT_INCLUDE} PUBLIC ${link_headers})
            endif ()
        endif ()
    endif ()


    if (LIBOPTS_DEFINITIONS)
        target_compile_definitions(${name} PUBLIC ${LIBOPTS_DEFINITIONS})
    endif ()

    if (LIBOPTS_OPTIONS)
        target_compile_options(${name} PUBLIC ${LIBOPTS_OPTIONS})
    endif ()

    if (LIBOPTS_DEPENDS)
        add_dependencies(${name} ${LIBOPTS_DEPENDS})
    endif ()
endfunction(tdaq_add_library)
